#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "arbres.h"


int est_feuille (t_ptr_arbre arbre) {
    return arbre->est_feuille;
}

t_ptr_arbre cree_feuille (int val) {
    t_ptr_arbre arbre;
    arbre = (t_ptr_arbre)malloc (sizeof (t_arbre));
    
    arbre->est_feuille = 1;
    arbre->d.valeur = val;
    
    return arbre;
}

t_ptr_arbre cree_noeud (t_ptr_arbre fg, t_ptr_arbre fd, char sym) {
    t_ptr_arbre arbre;
    arbre = (t_ptr_arbre) malloc (sizeof (t_arbre));
    
    arbre->est_feuille = 0;
    arbre->d.etiq = sym;
    arbre->fg = fg;
    arbre->fd = fd;
    
    return arbre;
}

t_ptr_arbre fils_gauche (t_ptr_arbre arbre) {
    assert (!est_feuille (arbre));
    return arbre->fg;
}

t_ptr_arbre fils_droit (t_ptr_arbre arbre) {
    assert (!est_feuille (arbre));
    return arbre->fd;
}

int valeur_feuille (t_ptr_arbre arbre) {
    assert (est_feuille (arbre));
    return arbre->d.valeur;
}

int etiq_noeud (t_ptr_arbre arbre) {
    assert (!est_feuille (arbre));
    return arbre->d.etiq;
}

int max (int a, int b) {
    return (a >= b) ? a : b;
}


/* Retourne le nombre de noeuds de l'arbre */
int nombre_noeuds (t_ptr_arbre arbre) {
    if (est_feuille (arbre))
        return 0;
    else
        return 1 + nombre_noeuds (fils_gauche (arbre)) + nombre_noeuds (fils_droit (arbre));
}

/* Retourne le nombre de feuilles de l'arbre */
int nombre_feuilles (t_ptr_arbre arbre) {
    if (est_feuille (arbre))
        return 1;
    else
        return nombre_feuilles (fils_gauche (arbre)) + nombre_feuilles (fils_droit (arbre));
}

/* Retourne la profondeur de l'arbre */
int profondeur (t_ptr_arbre arbre) {
    if (est_feuille (arbre))
        return 0;
    else
        return 1 + max (profondeur (fils_gauche (arbre)), profondeur (fils_droit (arbre)));
}

/* Retourne la hauteur de l'arbre */
int hauteur (t_ptr_arbre arbre) {
    if (est_feuille (arbre))
        return 1;
    else
        return 1 + max (hauteur (fils_gauche (arbre)), hauteur (fils_droit (arbre)));
}


/* Parcours prefixe de l'arbre */
void prefixe (t_ptr_arbre arbre) {
    prefixe_rec (arbre, 0);
}

void prefixe_rec (t_ptr_arbre arbre, int fd_ou_non) {
    if (est_feuille (arbre))
        if (fd_ou_non)
            printf (" %d", valeur_feuille (arbre));
        else
            printf ("%d", valeur_feuille (arbre));
    else {
        printf ("%c", etiq_noeud (arbre));
        prefixe_rec (fils_gauche (arbre), 0);
        prefixe_rec (fils_droit (arbre), 1);
    }
}

/* Parcours postfixe de l'arbre */
void postfixe (t_ptr_arbre arbre) {
    postfixe_rec (arbre, 0);
}

void postfixe_rec (t_ptr_arbre arbre, int fg_ou_non) {
    if (est_feuille (arbre)) {
        printf ("%d", valeur_feuille (arbre));
        if (fg_ou_non)
            printf (" ");
    }
    else {
        postfixe_rec (fils_gauche (arbre), 1);
        postfixe_rec (fils_droit (arbre), 0);
        printf ("%c", etiq_noeud (arbre));
    }
}

/* Parcours infixe de l'arbre */
int prec_op (char s) {
    return (s == '+') ? 0 : 1;
}

void infixe (t_ptr_arbre arbre) {
    infixe_rec (arbre, 0);
}

void infixe_rec (t_ptr_arbre arbre, int prec) {
    if (est_feuille (arbre))
        printf ("%d", valeur_feuille (arbre));
    else {
        if (prec_op (etiq_noeud (arbre)) < prec)
            printf ("(");;
        
        infixe_rec (fils_gauche (arbre), prec_op (etiq_noeud (arbre)));
        printf ("%c", etiq_noeud (arbre));
        infixe_rec (fils_droit (arbre), prec_op (etiq_noeud (arbre)));
        
        if (prec_op (etiq_noeud (arbre)) < prec)
            printf (")");
    }
}


/* Désérialisation pour le parcours prefixe */
t_ptr_arbre deserialisation_prefixe (void) {
    int valeur;
    char etiq;
    
    if (scanf ("%d", &valeur) == 0) {
        scanf ("%c", &etiq);
        return cree_noeud (deserialisation_prefixe (),
                           deserialisation_prefixe (),
                           etiq);
    }
    else
        return cree_feuille (valeur);
}
