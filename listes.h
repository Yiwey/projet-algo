#ifndef __LISTES_H__
#define __LISTES_H__

//Structure
typedef struct mm {
    int val;
    struct mm * suiv;
} t_maillon, * ptl;

//Déclarations
ptl cree_vide ();
int est_vide (ptl l);
int tete (ptl l);
ptl queue (ptl l);
ptl ajout (int e, ptl l);
int lgr (ptl l);
ptl copie (ptl l);
ptl rech (int X, ptl l);
ptl supp (int X, ptl l);
ptl ins (int X, ptl l);
ptl invl (ptl l);
void affiche (ptl l);

#endif