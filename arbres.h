

#ifndef __ARBRE_H__
#define __ARBRE_H__

/* Structure de données */
typedef struct moi_meme {
    int est_feuille;
    union data {
        int valeur;
        char etiq;
    } d;
    struct moi_meme * fg;
    struct moi_meme * fd;
} t_arbre, * t_ptr_arbre;

/* Prototypes */
int est_feuille (t_ptr_arbre arbre);
t_ptr_arbre cree_feuille (int val);
t_ptr_arbre cree_noeud (t_ptr_arbre fg, t_ptr_arbre fd, char sym);
t_ptr_arbre fils_gauche (t_ptr_arbre arbre);
t_ptr_arbre fils_droit (t_ptr_arbre arbre);
int valeur_feuille (t_ptr_arbre arbre);
int etiq_noeud (t_ptr_arbre arbre);
int max (int a, int b);

int nombre_noeuds (t_ptr_arbre arbre);
int nombre_feuilles (t_ptr_arbre arbre);
int profondeur (t_ptr_arbre arbre);
int hauteur (t_ptr_arbre arbre);

void prefixe (t_ptr_arbre arbre);
void prefixe_rec (t_ptr_arbre arbre, int fd_ou_non);

void postfixe (t_ptr_arbre arbre);
void postfixe_rec (t_ptr_arbre arbre, int fg_ou_non);

int prec_op (char s);
void infixe (t_ptr_arbre arbre);
void infixe_rec (t_ptr_arbre arbre, int prec);

t_ptr_arbre deserialisation_prefixe (void);

#endif
