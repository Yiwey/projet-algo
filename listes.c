#include <iostream>
#include "listes.h"
#include <assert.h>
#include <stdlib.h>
using namespace std;

//cree une liste vide
ptl cree_vide ()
{
  return (ptl)NULL;
}

int est_vide (ptl l)
{
  return ( l == cree_vide() );
}

int tete (ptl l)
{
  assert (!est_vide(l));
  return l->val;
}

ptl queue ( ptl l )
{
  assert (!est_vide(l));
  return l->suiv;
}

ptl ajout (int e, ptl l)
{
  ptl aux;
  aux = (ptl)malloc(sizeof(t_maillon));
  aux->val=e;
  aux->suiv=l;
  return aux;
}

int lgr (ptl l)
{
  if(est_vide(l)) return 0;
  else return(1+lgr(queue(l)));
}

ptl copie (ptl l)
{
  if(est_vide(l)) return (l);
  else return (ajout(tete(l), copie(queue(l))));
}

ptl rech (int X, ptl l)
{
  if (est_vide(l)) return (l);
  else if(tete(l) == X) return (l);
  else return(rech(X,queue(l)));
}

ptl supp(int X, ptl l)
{
  if(est_vide(l)) return (l);
  else 
  {
    if(X==tete(l)) return (supp(X,queue(l)));
    else return (ajout(tete(l), supp(X, queue(l))));
  }
}

ptl ins (int X, ptl l)
{
  ptl nw = (ptl)malloc(sizeof(t_maillon));
  nw->val = X;
  if(est_vide(l)||tete(l) >= X)
  {
    nw->suiv = l;
    return nw;
  }
  else
  {
    ptl aux = l;
    while (aux->suiv && aux->suiv->val < X)
    {
      aux = aux->suiv;
    }
    nw->suiv = aux->suiv;
    aux->suiv = nw;
    return l;
  }
}


ptl invl (ptl l)
{
  ptl r = (ptl)NULL;
  while(l)
  {
    ptl nl = l;
    l->suiv = r;
    r=l;
    l = nl;
  }
  
  return r;
}

void affiche (ptl l) 
{
    cout << "Liste :" <<endl;
    
    while (!est_vide (l)) 
    {
        cout << tete (l);
        if (!est_vide (queue (l))) cout << " --> ";
        
        l = queue (l);
    }
    
    cout << endl;
}
